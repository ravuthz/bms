<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TimeRequest;
use App\Models\Time;

class TimeController extends Controller {

    // view
    public function index() {
        
        $data['title'] = 'time index';
        $data['times'] = Time::all()->toArray();
        return view('dash.time.index', $data);
    }


    // view
    public function create() {
        return view('dash.time.create');
    }

    // action
    public function store(TimeRequest $request) {
        Time::create($request->all());
        return redirect('dash/time');
    }

    // view detail
    public function show($id)
    {
        return view('dash.time.update', $id);
    }

    // vie
    public function edit($id)
    {
        // echo "Edit;";
        // dd(Time::find($id)->toArray());
        // $data['id'] = Time::find($id);
        $data['time'] = Time::find($id);

        return view('dash.time.edit', $data);
    }

    // aciton
    public function update(TimeRequest $request, $id)
    {
        //
        $time = Time::findOrFail($id);
        $time->update($request->all());
        return redirect('dash/time');
    }

    // action
    public function destroy($id)
    {
        //
        $time = Time::findOrFail($id);
        $time->delete($id);
        return redirect('dash/time');
    }
}
