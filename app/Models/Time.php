<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Time extends Model {
    public $table = 'tbl_times';

    protected $fillable = [
        'time'
    ];
    
    public function scopePublished($query){
        $query->where('created_at','<=', Carbon::now());
    }

    public function departure(){
    	return $this->belongsTo('App\Models\Departure');
    }
    
}
