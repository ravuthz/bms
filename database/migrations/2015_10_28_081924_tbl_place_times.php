<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPlaceTimes extends Migration {

    public function up() {
        Schema::create('tbl_place_times', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('place_from_id')->unsigned();
            $table->integer('place_to_id')->unsigned();
            $table->integer('time_from_id')->unsigned();
            $table->integer('time_to_id')->unsigned();


        //     $table->foreign('place_from_id')->references('id')->on('tbl_places')->onDelete('cascade');
        //     $table->foreign('place_to_id')->references('id')->on('tbl_places')->onDelete('cascade');

        //     $table->foreign('time_from_id')->references('id')->on('tbl_times')->onDelete('cascade');
        //     $table->foreign('time_to_id')->references('id')->on('tbl_times')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::drop('tbl_place_times');
    }

}

// => Illuminate\Database\Eloquent\Relations\HasMany {#666}
// >>> $pt->times()->first();
// Illuminate\Database\QueryException with message 'SQLSTATE[42S22]: Column not found: 1054 Unknown column 'tbl_times.place_times_id' in 'where clause' (
// SQL: select * from `tbl_times` where `tbl_times`.`place_times_id` = 1 and `tbl_times`.`place_times_id` is not null limit 1)'
