<?php

use Illuminate\Database\Seeder;

class BookDetailTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('tbl_book_details')->insert([
            'booking_id' => 1,
            'bus_id' => 1,
            'seat_id' => 1
        ]);
    }
}
