<?php

use Illuminate\Database\Seeder;

class PlaceTimesTableSeeder extends Seeder {

    public function run() {
        DB::table('tbl_place_times')->insert([
            'place_from_id' => 1,
            'place_to_id' => 2,
            'time_from_id' => 1,
            'time_to_id' => 2
        ]);
        DB::table('tbl_place_times')->insert([
            'place_from_id' => 1,
            'place_to_id' => 3,
            'time_from_id' => 1,
            'time_to_id' => 2
        ]);
        DB::table('tbl_place_times')->insert([
            'place_from_id' => 1,
            'place_to_id' => 4,
            'time_from_id' => 1,
            'time_to_id' => 2
        ]);
    }

}
