<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Bus;
class BusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bus::class, 10)->create();

        // for($i=0 ; $i<10 ; $i++){
        //     DB::table('tbl_buses')->insert([

        //         'code' => rand (10000000 , 99999999),
        //         'driver_id' => rand (1 , 9),
        //         'park' => str_random(10).'',
        //         'type' => rand (1 , 9),
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now()
        //     ]);
        // }
    }
}
