@extends('dash')

@section('title', 'Create Branch')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Insert Branch Information</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">

            {!! Form::open(['action'=>['Dash\BranchController@store'], 'class'=>'form floating-label']) !!}
                @include('dash.branch.form', ['submitText' => 'Create Branch'])
            {!! Form::close() !!}
            <hr/><br/>
        </div>
    </div>

@stop