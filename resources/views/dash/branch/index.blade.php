@extends('dash')

@section('title', 'Admin')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List all Branchs Information</h1>
        </div>
    </div>

    <table id="tblBranches" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Control By</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Name</td>
                <td>Address</td>
                <td>Control By</td>
                <td>Created At</td>
                <td>Updated At</td>
                <td>Action</td>
            </tr>
        </tbody>
    </table>

    <script type="text/javascript">
        // var link = "http://localhost:8000/dashboard/branch/";

        $(function(){



            var tblBranch = $('#tblBranches').DataTable({
                ajax: 'http://localhost:8000/dashboard/branch/json',
                columns: [
                    {data: 'name'},
                    {data: 'address'},
                    {data: 'control_by'},
                    {data: 'created_at'},
                    {data: 'updated_at'},
                    {data: ' '},
                ],
                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        defaultContent: "<button class='btn btn-xs btn-info btnModify'><span class='glyphicon glyphicon-edit'></button><button class='btn  btn-xs btn-danger btnDelete'><span class='glyphicon glyphicon-trash'></button>"
                    }
                ]
            });


            // $.get('http://localhost:8000/dashboard/branch/json', function(data){
            //     console.log(data);

            //     var tblBranch = $('#tblBranches').DataTable({
            //         data: data,
            //         columns: [
            //             {data: 'name'},
            //             {data: 'address'},
            //             {data: 'control_by'},
            //             {data: 'created_at'},
            //             {data: 'updated_at'},
            //             {data: ' '},
            //         ],
            //         columnDefs: [
            //             {
            //                 targets: -1,
            //                 data: null,
            //                 defaultContent: "<button class='btn btn-xs btn-info btnModify'><span class='glyphicon glyphicon-edit'></button><button class='btn  btn-xs btn-danger btnDelete'><span class='glyphicon glyphicon-trash'></button>"
            //             }
            //         ]
            //     });
            // });



        });
    </script>

@stop