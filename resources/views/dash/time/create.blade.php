@extends('dash')

@section('content')

<h1>Create New Time</h1>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-8">
                {!! Form::open(['action' => 'Dash\TimeController@store', 'class'=>'form']) !!}
                	@include('dash.time.form', ['submitText' => 'Create Time'])
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection