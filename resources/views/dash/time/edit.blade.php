@extends('dash')

@section('content')

<h1>Update Time</h1>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-8">

                

                {!! Form::model($time, ['route' => ['dash.time.update', $time->id ], 'method' => 'PATCH' ]) !!}


                	@include('dash.time.form', ['submitText' => 'Update Time', 'id'=>'submitText'])
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop


