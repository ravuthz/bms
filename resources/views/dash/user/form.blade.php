<div class="form-group">
    {!! Form::label('firstname','First Name') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example Vichet.</p>
</div>
<div class="form-group">
    {!! Form::label('lastname','Last Name') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example Sai.</p>
</div>
<div class="form-group">
    {!! Form::label('username','Username') !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example saivichet.</p>
</div>
<div class="form-group">

    {!! Form::label('email','Examplemail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example email@example.com</p>
</div>
<div class="form-group">

    {!! Form::label('password','Password') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('password_confirmation','Confirm Password') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('type','Type') !!}
    @if(!isset($type))
        {!! Form::select('type', $types, 2, ['class' => 'form-control']) !!}
    @else
        {!! Form::select('type', $types, $type, ['class' => 'form-control']) !!}
    @endif
    {{-- {!! Form::select('product_id', $productList, null, array('class' => 'form-control')) !!} --}}
</div>
<div class="form-group">
    {!! Form::label('phone','Phone') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('address','Address') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('card_no','Card Number') !!}
    {!! Form::text('card_no', null, ['class' => 'form-control']) !!}
    <p class="help-block">Identify Card.</p>
</div>

{!! Form::submit($submittype, ['class' => 'btn btn-primary form-control']) !!}
