@extends('dash')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Users</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @foreach($users as $user)
                <article>

                    <a href=""><h2>{{$user->username}}</h2></a>
                    {{-- href="{url('/articles',[$article->id])}}" --}}
                    <div class="body">{{$user->email}}</div>
                </article>
            @endforeach
        </div>
    </div>

@stop
