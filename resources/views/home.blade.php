<!DOCTYPE HTML>
<html>
<head>
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="BMS, Bus Management System" />

    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    {{-- <link rel="stylesheet" type="text/css" href="{{ url(elixir('css/all.css')) }}"> --}}


    <script type='text/javascript' src="js/jquery-1.11.1.min.js"></script>

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <!-- start menu -->
    <link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="js/megamenu.js"></script>
    <script>
    $(document).ready(function() {
        $(".megamenu").megamenu();
    });
    </script>
    {{-- // <script type="text/javascript" src="js/bootstrap.min.js"></script> --}}
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>

    <script src="js/menu_jquery.js"></script>

    <script src="js/simpleCart.min.js"></script>
    <script src="js/responsiveslides.min.js"></script>

    <script>
    // You can also use "$(window).load(function() {"
    $(function() {
        // Slideshow 1
        $("#slider1").responsiveSlides({
            auto: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
        });
    });

    </script>
</head>

<body>
    <!-- header -->
    <div class="top_bg">
        <div class="container">
            <div class="header_top-sec">
                <div class="top_right">
                    <ul>
                        <li><a href="/about">About</a></li> | <li><a href="/contact">Contact</a></li>
                    </ul>
                </div>
                <div class="top_left">
                    <ul>
                        <li class="top_link">Email:<a href="mail_to_me@bms.com">mail_to_me@bms.com</a></li>|
                        <li class="top_link"><a href="/auth/login">My Account</a></li>|
                    </ul>
                    <div class="social">
                        <ul>
                            <li><a href="#"><i class="facebook"></i></a></li>
                            <li><a href="#"><i class="twitter"></i></a></li>
                            <li><a href="#"><i class="dribble"></i></a></li>
                            <li><a href="#"><i class="google"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

    <div class="header_top">
        <div class="container">
            <div class="logo">
                <a href="/"><img src="images/logo.png" alt="" /></a>
            </div>
            <div class="header_right">
                <div class="login">
                    <a href="/auth/login">LOGIN</a>
                </div>
                {{-- <div class="cart box_1"> --}}
                    {{-- <a href="cart.html">
                        <h3> <span class="simpleCart_total">$0.00</span> (<span id="simpleCart_quantity" class="simpleCart_quantity">0</span> items)<img src="images/bag.png" alt=""></h3>
                    </a> --}}
                    {{-- <p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p> --}}
                    {{-- <div class="clearfix"> </div> --}}
                {{-- </div> --}}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="mega_nav">
        <div class="container">
            <div class="menu_sec">
                <!-- start header menu -->
                <ul class="megamenu skyblue">
                    <li class="showhide" style="display: none;">
                        <span class="title">MENU</span>
                        <span class="icon1"></span>
                        <span class="icon2"></span>
                    </li>

                    <li id='home'><a class="color1" href="/">Home</a></li>
                    <li id='branches'><a class="color2" href="/branches">branches</a></li>
                    <li id='services'><a class="color3" href="/services">services</a></li>
                    <li id='promotion'><a class="color4" href="/promotion">promotion</a></li>
                </ul>
                <div class="search">
                    <form>
                        <input type="text" value="" placeholder="Search...">
                        <input type="submit" value="">
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="content">
        </br>
        @yield('content')
    </div>

    <div class="bottom_content">
        @yield('bottom_content')
    </div>


    <div class="recommendation">
        <div class="container">
            <div class="recmnd-head">
                <h3>RECOMMENDATIONS FOR YOU</h3>
            </div>
            <div class="bikes-grids">
                <ul id="flexiselDemo1">
                    @for ($i=1; $i<=7; $i++)
                        <li>
                            <a href="products.html"><img src="images/bus/{{$i}}.jpg" alt="" /></a>
                            <h4><a href="products.html">Our Bus</a></h4>
                        </li>
                    @endfor

                </ul>
                <script type="text/javascript">
                $(window).load(function() {
                    $("#flexiselDemo1").flexisel({
                        visibleItems: 5,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 480,
                                visibleItems: 1
                            },
                            landscape: {
                                changePoint: 640,
                                visibleItems: 2
                            },
                            tablet: {
                                changePoint: 768,
                                visibleItems: 3
                            }
                        }
                    });
                });
                </script>
                <script type="text/javascript" src="js/jquery.flexisel.js"></script>
            </div>
        </div>
    </div>
    <!---->
    <div class="footer-content">
        <div class="container">
            <div class="ftr-grids">
                <div class="col-md-4 ftr-grid">
                    <h4>About Us</h4>
                    <ul>
                        <li><a href="/contact/#who_we_are">Who We Are</a></li>
                        <li><a href="/contact/#us">Contact Us</a></li>
                        <li><a href="/about/#our_team">Our Team</a></li>
                    </ul>
                </div>
                <div class="col-md-4 ftr-grid">
                    <h4>Customer service</h4>
                    <ul>
                        <li><a href="#">Booking</a></li>
                        <li><a href="#">Cancellation</a></li>
                        <li><a href="#">Destination</a></li>
                    </ul>
                </div>
                <div class="col-md-4 ftr-grid">
                    <h4>Your account</h4>
                    <ul>
                        <li><a href="account.html">Register</a></li>
                        <li><a href="#">Detail Activities</a></li>
                        <li><a href="#">Detail Information</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!---->
    <div class="footer">
        <div class="container">
            <div class="store">
                <ul>
                    <li class="active">PROVINCES:</li>
                    <?php $places = App\Models\Place::all()->toArray(); ?>
                    @foreach ($places as $place)
                        <li><a href="#">{{ $place['name'] }}</a></li>
                        @if ($place != end($places))
                            <li><a href="#">|</a></li>
                        @endif
                    @endforeach

                </ul>
            </div>
            <div class="copywrite">
                <p>Copyright © 2015 Furnyish Store All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var url = window.location.href;
        if (/\/branches/.test(url)) {
            $('.megamenu .skyblue li').removeClass('active');
            $('#branches').addClass('active');
        } else if (/\/services/.test(url)) {
            $('.megamenu .skyblue li').removeClass('active');
            $('#services').addClass('active');
        } else if (/\/promotion/.test(url)) {
            $('.megamenu .skyblue li').removeClass('active');
            $('#promotion').addClass('active');
        } else {
            $('.megamenu .skyblue li').removeClass('active');
            $('#home').addClass('active');
        }
    </script>
    </div>
    <!---->
</body>

</html>
